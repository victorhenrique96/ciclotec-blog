<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define( 'DB_NAME', 'ciclotec' );

/** Usuário do banco de dados MySQL */
define( 'DB_USER', 'ciclotec' );

/** Senha do banco de dados MySQL */
define( 'DB_PASSWORD', '123456' );

/** Nome do host do MySQL */
define( 'DB_HOST', 'localhost' );

/** Charset do banco de dados a ser usado na criação das tabelas. */
define( 'DB_CHARSET', 'utf8mb4' );

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '?7e?&` 5DdMr3[=*&tUy7zc94in]~gV@:6N~^Ha<@D$XU=$Hk }[=FmKqj+&bcDo' );
define( 'SECURE_AUTH_KEY',  'w-iMEcHLD5s^*ggqL_>Xy2 uslj4&v=x (QA,mf#`K,{et+.<}5#c`UPetg#MIZq' );
define( 'LOGGED_IN_KEY',    'lL~V:qW;1jKWxW&wWrj,?h0(bN/itBG-7smX/2n3^ICL* b%G?tvG5Y@F_j,?|hH' );
define( 'NONCE_KEY',        '5 xW]?Xd?E#oW}lj>u].+Sk;SL|QY>@F<)Q<0Y@C~7s {C>Y)+TvN/L&}A#yN-A.' );
define( 'AUTH_SALT',        'Ckzu$TQH?L_p40|oSbS9K5`Nk$-QOmiaH}%3_bPa.*7e}zs%302Q}tPdXLw2U%%_' );
define( 'SECURE_AUTH_SALT', 'p`uFit8s`M,s^b%!1l5m/aD$q91nD|*Y6s}Y|_oXiY(t2cgKD>_E,aHd#*O?)Hsv' );
define( 'LOGGED_IN_SALT',   ';bys%c]f$(E|`gTtpCcjbw9G)3$_%<3W!6oSwd{?zKW~*}L#(7@LKIyZIO7kUk-H' );
define( 'NONCE_SALT',       'aUQew|%^6KRCIM(*yDl_x,pQ!H4[{;Lz]7gmR5*O45HV#C-JCG$zJK$+is1NtlDa' );

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix = 'wp_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');
