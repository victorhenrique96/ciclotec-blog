<?php
/**
 * The template part for displaying a message that posts cannot be found.
 *
 *
 * Please browse readme.txt for credits and forking information
 * @package photoblogster
 */

?>

<section class="no-results not-found">
	<header class="page-header">
		<span class="screen-reader-text"><?php esc_html_e( 'Nada encontrado!', 'photoblogster' ); ?></span>
		<h1 class="page-title"><?php esc_html_e( 'Nada encontrado!', 'photoblogster' ); ?></h1>
	</header><!-- .page-header -->

	<div class="page-content">
		<?php if ( is_home() && current_user_can( 'publish_posts' ) ) : ?>

			<p><?php printf( wp_kses( __( 'Pronto para o seu primeiro Post? <a href="%1$s">Comece aqui</a>.', 'photoblogster' ), array( 'a' => array( 'href' => array() ) ) ), esc_url( admin_url( 'post-new.php' ) ) ); ?></p>

		<?php elseif ( is_search() ) : ?>

			<p><?php esc_html_e( 'Desculpe, mas não encontramos resultados para sua pesquisa. Tente novamente com outra palavra-chave.', 'photoblogster' ); ?></p>
			<?php get_search_form(); ?>

		<?php else : ?>

			<p><?php esc_html_e( 'Parece que não encontramos resultados para o que você está pesquisando.', 'photoblogster' ); ?></p>
			<?php get_search_form(); ?>

		<?php endif; ?>
	</div><!-- .page-content -->
</section><!-- .no-results -->
