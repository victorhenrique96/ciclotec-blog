<?php

/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * Please browse readme.txt for credits and forking information
 * @package photoblogster
 */

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>

<head>
  <meta charset="<?php bloginfo('charset'); ?>" />
  <meta name="viewport" content="width=device-width" />
  <link rel="profile" href="http://gmpg.org/xfn/11" />
  <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
  <?php wp_head(); ?>
  <!-- Google Tag Manager -->
  <script>
    (function(w, d, s, l, i) {
      w[l] = w[l] || [];
      w[l].push({
        'gtm.start': new Date().getTime(),
        event: 'gtm.js'
      });
      var f = d.getElementsByTagName(s)[0],
        j = d.createElement(s),
        dl = l != 'dataLayer' ? '&l=' + l : '';
      j.async = true;
      j.src =
        'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
      f.parentNode.insertBefore(j, f);
    })(window, document, 'script', 'dataLayer', 'GTM-T2H6QW7');
  </script>
  <!-- End Google Tag Manager -->

</head>

<body <?php body_class(); ?>>

  <!-- Google Tag Manager (noscript) -->
  <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-T2H6QW7" ; height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
  <!-- End Google Tag Manager (noscript) -->

  <div id="page" class="hfeed site">
  <header id="masthead" class="<?php if(!is_home()){ echo 'header-integra';}?>">
      <nav class="navbar lh-nav-bg-transform navbar-default navbar-fixed-top navbar-left">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="container" id="navigation_menu">
          <div class="navbar-header">
            <?php if (has_nav_menu('primary')) { ?>
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only"><?php echo esc_html('Toggle Navigation', 'photoblogster') ?></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
            <?php } ?>
            <a href="<?php echo esc_url(home_url('/')); ?>">
              <?php
              if (!has_custom_logo()) {
                echo '<div class="navbar-brand">';
                bloginfo('name');
                echo '</div>';
              } else {
                the_custom_logo();
              } ?>
            </a>
          </div>
          <?php if (has_nav_menu('primary')) {
            photoblogster_header_menu(); // main navigation 
          }
          ?>

        </div>
        <!--#container-->
      </nav>
      <?php if (is_front_page()) { ?>
        <div class="site-header">
          <div class="container-fluid">
            <div class="container">
              <div class="site-branding">
                <span class="home-link">
                  <span class="site-blog">Blog</span>
                  <span class="frontpage-site-title site-title"><?php bloginfo('name'); ?></span>
                  <div class="home-inputs">
                    <select class="input-category" name="categorias" onchange="location = this.value;">
                      <option value="Categoria" selected>Categoria</option>
                      <?php
                      $args=array(
                      'orderby' => 'name',
                      'order' => 'ASC'
                      );
                      $categories=get_categories($args);
                      foreach($categories as $category) {
                      echo '<option value="' . get_category_link( $category->term_id ) . '" title="' . sprintf( __( "Ver postagens na categoria: %s" ), $category->name ) . '" ' . '>' . $category->name.'</option> ';}
                      ?> 
                    </select>
                    <label>

                    <form role="search" method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
                      <label>
                        <span class="screen-reader-text"><?php echo esc_html( 'Search for:', 'label', 'photoblogster' ); ?></span>
                        <input type="search" class="search-field" placeholder="<?php echo esc_html( 'Faça sua busca', 'placeholder', 'photoblogster' ); ?>" value="<?php echo get_search_query(); ?>" name="s" title="<?php echo esc_attr( 'Search for:', 'label', 'photoblogster' ); ?>" required onfocus="this.value=''">
                      </label>
                      <button type="submit" class="search-submit"></button>
                    </form>
                    
                  </div>
              </div>
            </div>
            </span>

          </div>
          <!--.site-branding (container-fluid) -->
        </div>
        <!--.site-header-->
      <?php } ?>


    </header>

    <div id="content" class="site-content">